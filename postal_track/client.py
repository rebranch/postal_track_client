import requests
import urlparse
from postal_track.exceptions import PostalTrackException


class PostalTrackApiClient(object):
    api_host = u'http://postal-track.redeploy.ru'
    # api_host = u'http://127.0.0.1:5000'
    register_url = u'/api/shipping/create/'
    get_data_url = u'/api/shipping/{}/'

    def __init__(self, key):
        self.key = key

    def register_shipping(self, track_number, service):
        url = urlparse.urljoin(self.api_host, self.register_url)
        response = requests.post(url, data={u'id': track_number, u'service': service}, params={u'key': self.key})
        response_decoded = response.json()
        if response_decoded.get(u'status') == u'success':
            return True
        else:
            raise PostalTrackException(response_decoded.get(u'error', u''))

    def get_shipping_data_statuses(self, track_number):
        get_data_url = self.get_data_url.format(track_number)
        url = urlparse.urljoin(self.api_host, get_data_url)
        response = requests.get(url, params={u'key': self.key})
        response_decoded = response.json()
        if response_decoded.get(u'status') == u'success':
            return response_decoded
        else:
            raise PostalTrackException(response_decoded.get(u'error'))