import datetime

from postal_track.client import PostalTrackApiClient


class TrackingStatus(object):
    status = None
    extended_status = None
    date = None
    postal_code = None

    def __init__(self, status, extended_status, date, postal_code):
        self.status = status
        self.extended_status = extended_status
        self.date = date
        self.postal_code = postal_code

    @classmethod
    def factory_from_response_dict(cls, response_dict):
        status = response_dict.get(u'status')
        extended_status = response_dict.get(u'extended_status')
        postal_code = response_dict.get(u'postal_code')
        try:
            date = datetime.datetime.strptime(response_dict.get(u'date', u''), u'%Y-%m-%dT%H:%M:%S.%f')
        except ValueError:
            date = None
        return cls(
            status=status,
            extended_status=extended_status,
            date=date,
            postal_code=postal_code
        )

    def __repr__(self):
        return '<TrackingStatus instance \'{}\'>'.format(str(self.postal_code))


class AbstractShipping(object):
    tracking_number = None
    last_checkup = None
    service = None

    def __init__(self, tracking_number, key):
        self.tracking_number = tracking_number
        self.client = PostalTrackApiClient(key=key)

    def register(self):
        return self.client.register_shipping(track_number=self.tracking_number, service=self.service)

    def get_statuses(self):
        response = self.client.get_shipping_data_statuses(track_number=self.tracking_number)
        try:
            self.last_checkup = datetime.datetime.strptime(response.get(u'last_checkup'), u'%Y-%m-%dT%H:%M:%S.%f')
        except ValueError:
            self.last_checkup = None
        statuses = [TrackingStatus.factory_from_response_dict(x) for x in response.get(u'statuses', [])]
        return statuses

    def __repr__(self):
        return '<Shipping instance \'{}\'>'.format(str(self.tracking_number))