from postal_track.base import AbstractShipping


class RussianPostShipping(AbstractShipping):
    service = u'russian_post'


class CDEKShipping(AbstractShipping):
    service = u'cdek'