from setuptools import setup, find_packages


setup(
    name='postal_track',
    version=0.1,
    author='Rebranch',
    packages=find_packages(),
    install_requires=[u'requests'],
    url='git@bitbucket.org:rebranch/postal_track_client.git'
)